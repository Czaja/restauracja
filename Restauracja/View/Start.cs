﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Restauracja.Data;
using System.Net.Mail;
using System.Net;

namespace Restauracja
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
        }

        private int x = 0;
        private int y = 0;
        private List<TextBox> listTextBox;
        List<Product> listProduct = new ProductFactory().getAll();
        private Label labelSum;
        private TextBox commentsToOrder;


        private Label CreateLabel(string text, string name, int _x, int _y, string fontType, float fontSize, System.Drawing.FontStyle fontStyle, int width)
        {
            Label label = new Label()
            {
                Text = text,
                Name = name,
                Location = new Point(_x, _y),
                Font = new Font(fontType, fontSize, fontStyle),
                Width = width
            };

            return label;
        }

        private Button CreateButton(string text, string name, int _x, int _y, int width)
        {
            Button button = new Button()
            {
                Text = text,
                Name = name,
                Location = new Point(_x, _y),
                Width = width
            };

            return button;
        }

        private TextBox CreateTextBox(string text, string name, int _x, int _y, string fontType, float fontSize, System.Drawing.FontStyle fontStyle, int width)
        {
            TextBox button = new TextBox()
            {
                Text = text,
                Name = name,
                Location = new Point(_x, _y),
                Font = new Font(fontType, fontSize, fontStyle),
                Width = width
            };

            return button;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnlEditEmail.Hide();
            pnlSuccess.Hide();
            pnlCreateCustomer.Hide();
            pnlHistoryOrder.Controls.Clear();

            y = 0;
            List<Order> listOrder = new OrderFactory().getAll();

            for (int i = 0; i < listOrder.Count; i++)
            {
                Label labelHeader = CreateLabel("Zamówienie " + (i + 1), "", x, y, "Arial", 12, FontStyle.Bold, 200);

                pnlHistoryOrder.Controls.Add(labelHeader);
                y += 20;

                string[] products = listOrder[i].Products.Split(',');
                string[] counts = listOrder[i].Counts.Split(',');

                float sum = 0.0f;
                for (int j = 0; j < products.Length - 1; j++)
                {
                    Product product = listProduct.Find(x => x.ID == int.Parse(products[j]));

                    Label labelProduct = CreateLabel(product.Name, product.ID + "_" + product.Name, x + 10, y, "Arial", 9, FontStyle.Regular, 150);
                    pnlHistoryOrder.Controls.Add(labelProduct);

                    Label labelCount = CreateLabel("Ilość: " + counts[j], product.ID + "_" + product.Name, x + 160, y, "Arial", 9, FontStyle.Regular, 60);
                    pnlHistoryOrder.Controls.Add(labelCount);

                    sum += (int.Parse(counts[j]) * product.Price);
                    Label labelPrice = CreateLabel((int.Parse(counts[j]) * product.Price) + " zł", product.ID + "_" + product.Name, x + 220, y, "Arial", 9, FontStyle.Regular, 50);
                    pnlHistoryOrder.Controls.Add(labelPrice);

                    if (products.Length - 2 == j)
                    {
                        Label labelSum = CreateLabel("Suma: " + sum + " zł", product.ID + "_" + product.Name, x + 270, y, "Arial", 9, FontStyle.Regular, 100);
                        pnlHistoryOrder.Controls.Add(labelSum);
                    }

                    y += 20;
                }
            }

            pnlHistoryOrder.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Category> listCategory = new CategoryFactory().getAll();
            listProduct = new ProductFactory().getAll();
            List<Order> listOrder = new OrderFactory().getAll();
            listTextBox = new List<TextBox>();

            pnlEditEmail.Hide();
            pnlHistoryOrder.Hide();
            pnlSuccess.Hide();
            pnlCreateCustomer.Hide();
            pnlCreateCustomer.Controls.Clear();
            y = 0;

            foreach (Category category in listCategory)
            {
                Label labelCategory = CreateLabel(category.Name, category.ID + "_" + category.Name, x, y, "Arial", 12, FontStyle.Bold, 200);
                pnlCreateCustomer.Controls.Add(labelCategory);
                y += 20;

                List<Product> productByCategory = listProduct.FindAll(x => x.Category == category.ID && x.TypeProduct == Constant.TypeProduct.Main);
                foreach (Product product in productByCategory)
                {
                    Label labelProduct = CreateLabel(product.Name, product.ID + "_" + product.Name, x + 10, y, "Arial", 9, FontStyle.Regular, 200);
                    pnlCreateCustomer.Controls.Add(labelProduct);

                    drawProduct(product);
                }

                productByCategory = listProduct.FindAll(x => x.Category == category.ID && x.TypeProduct == Constant.TypeProduct.Extra);
                bool isFirst = true;
                foreach (Product product in productByCategory)
                {
                    if (isFirst)
                    {
                        Label labelSubCategory = CreateLabel("Dodatki do " + category.Name, category.ID + "_" + category.Name + "_" + "E", x, y, "Arial", 11, FontStyle.Bold, 200);

                        isFirst = false;
                        pnlCreateCustomer.Controls.Add(labelSubCategory);
                        y += 20;
                    }

                    Label labelProduct = CreateLabel(product.Name, product.ID + "_" + product.Name, x + 10, y, "Arial", 9, FontStyle.Regular, 200);
                    pnlCreateCustomer.Controls.Add(labelProduct);

                    drawProduct(product);
                }

            }

            Label label = CreateLabel("Uwagi do zamówienia:", "Suma", x + 10, y, "Arial", 9, FontStyle.Regular, 160);
            pnlCreateCustomer.Controls.Add(label);

            labelSum = CreateLabel("Suma: " + 0 + " zł", "Suma", x + 260, y, "Arial", 9, FontStyle.Regular, 80);

            pnlCreateCustomer.Controls.Add(labelSum);
            y += 30;

            commentsToOrder = CreateTextBox("", "commentsToOrder", x + 10, y, "Arial", 9, FontStyle.Regular, 200);
            pnlCreateCustomer.Controls.Add(commentsToOrder);

            Button createOrderButton = CreateButton("Zamów", "createOrder", x + 260, y, 80);
            createOrderButton.Click += buttonCreateOrder_Click;
            pnlCreateCustomer.Controls.Add(createOrderButton);
            y += 20;

            pnlCreateCustomer.Show();
        }

        private void drawProduct(Product product)
        {
            Label labelPrice = CreateLabel(product.Price + " zł", product.ID + "_" + product.Name, x + 210, y, "Arial", 9, FontStyle.Regular, 50);
            pnlCreateCustomer.Controls.Add(labelPrice);

            Button leftButton = CreateButton("<", "textBox_" + product.ID, x + 260, y, 20);
            leftButton.Click += buttonLeft_Click;
            pnlCreateCustomer.Controls.Add(leftButton);

            TextBox textBoxCount = CreateTextBox("0", "textBox_" + product.ID, x + 280, y, "Arial", 9, FontStyle.Regular, 40);
            textBoxCount.KeyPress += textBox1_KeyPress;
            listTextBox.Add(textBoxCount);
            pnlCreateCustomer.Controls.Add(textBoxCount);

            Button rightButton = CreateButton(">", "textBox_" + product.ID, x + 320, y, 20);
            rightButton.Click += buttonRight_Click;
            pnlCreateCustomer.Controls.Add(rightButton);
            y += 20;
        }

        private void buttonRight_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            TextBox textBox = listTextBox.Find(x => x.Name.Equals(btn.Name));

            if (!String.IsNullOrEmpty(textBox.Text))
            {
                int i = int.Parse(textBox.Text);
                i++;
                textBox.Text = i.ToString();
            }
            else
            {
                textBox.Text = "0";
            }

            calculatedSum();
        }

        private void calculatedSum()
        {
            float sum = 0.0f;

            foreach (TextBox textBox in listTextBox)
            {
                String[] str = textBox.Name.Split('_');
                Product product = listProduct.Find(x => x.ID == int.Parse(str[1]));
                if(!String.IsNullOrEmpty(textBox.Text))
                {
                    sum += (product.Price * int.Parse(textBox.Text));
                }
            }

            labelSum.Text = "Suma: " + sum + " zł";
        }

        private void buttonLeft_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            TextBox textBox = listTextBox.Find(x => x.Name.Equals(btn.Name));

            if (!String.IsNullOrEmpty(textBox.Text))
            {
                int i = int.Parse(textBox.Text);
                if (i > 0)
                {
                    i--;
                    textBox.Text = i.ToString();
                }
            }
            else
            {
                textBox.Text = "0";
            }

            calculatedSum();
        }

        private void buttonCreateOrder_Click(object sender, EventArgs e)
        {
            String products = "";
            String counts = "";

            foreach (TextBox textBox in listTextBox)
            {
                String[] str = textBox.Name.Split('_');

                if (!String.IsNullOrEmpty(textBox.Text))
                {
                    if (int.Parse(textBox.Text) > 0)
                    {
                        products += str[1] + ",";
                        counts += textBox.Text + ",";
                    }
                }
            }

            pnlEditEmail.Hide();
            pnlHistoryOrder.Hide();
            pnlCreateCustomer.Hide();
            pnlSuccess.Show();

            new OrderFactory().create(new Order(-1, products, counts, commentsToOrder.Text, false));

            List<Email> email = new EmailFactory().getAll();

            string[] productsTab = products.Split(',');
            string[] countsTab = counts.Split(',');
            string body = "";

            float sum = 0.0f;

            for (int j = 0; j < productsTab.Length - 1; j++)
            {
                Product product = listProduct.Find(x => x.ID == int.Parse(productsTab[j]));

                body += product.Name + " ";
                body += "Ilość: " + countsTab[j] + " ";
                body += (int.Parse(countsTab[j]) * product.Price) + " zł\n";
                sum += (int.Parse(countsTab[j]) * product.Price);

                if (productsTab.Length - 2 == j)
                {
                    body += "Suma: " + sum + " zł\n";
                }
            }

            body += "Uwagi do zamówienia:\n";
            body += commentsToOrder.Text;

            try {
                var fromAddress = new MailAddress(email[0].From, "From Name");
                var toAddress = new MailAddress(email[0].To, "To Name");
                string fromPassword = email[0].Password;
                string subject = email[0].Subject;

                var smtp = new SmtpClient
                {
                    Host = email[0].Host,
                    Port = int.Parse(email[0].Port),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            } catch {

            }
        }

        private void createLabel()
        {

        }

        private void pnlCreateCustomer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

            calculatedSum();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            pnlCreateCustomer.Hide();
            pnlHistoryOrder.Hide();
            pnlEditEmail.Show();

            List<Email> email = new EmailFactory().getAll();

            textBoxSMTP.Text = email[0].Host;
            textBoxPort.Text = email[0].Port;
            textBoxTo.Text = email[0].To;
            textBoxFrom.Text = email[0].From;
            textBoxSubject.Text = email[0].Subject;
            textBoxPassword.Text = email[0].Password;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            new EmailFactory().update(new Email(-1, textBoxSMTP.Text, textBoxPort.Text, textBoxTo.Text, textBoxFrom.Text, textBoxSubject.Text, textBoxPassword.Text));
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
