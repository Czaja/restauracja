﻿namespace Restauracja
{
    partial class Start
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateOrder = new System.Windows.Forms.Button();
            this.btnHistory = new System.Windows.Forms.Button();
            this.pnlStart = new System.Windows.Forms.Panel();
            this.btnEditEmail = new System.Windows.Forms.Button();
            this.pnlCreateCustomer = new System.Windows.Forms.Panel();
            this.pnlSuccess = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlHistoryOrder = new System.Windows.Forms.Panel();
            this.pnlEditEmail = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxSubject = new System.Windows.Forms.TextBox();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.textBoxFrom = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.textBoxSMTP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.pnlStart.SuspendLayout();
            this.pnlSuccess.SuspendLayout();
            this.pnlEditEmail.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreateOrder
            // 
            this.btnCreateOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreateOrder.Location = new System.Drawing.Point(3, 3);
            this.btnCreateOrder.Name = "btnCreateOrder";
            this.btnCreateOrder.Size = new System.Drawing.Size(148, 55);
            this.btnCreateOrder.TabIndex = 0;
            this.btnCreateOrder.Text = "Stwórz zamówienie";
            this.btnCreateOrder.UseVisualStyleBackColor = true;
            this.btnCreateOrder.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnHistory
            // 
            this.btnHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHistory.Location = new System.Drawing.Point(157, 3);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(151, 55);
            this.btnHistory.TabIndex = 1;
            this.btnHistory.Text = "Historia zamówień";
            this.btnHistory.UseVisualStyleBackColor = true;
            this.btnHistory.Click += new System.EventHandler(this.button2_Click);
            // 
            // pnlStart
            // 
            this.pnlStart.Controls.Add(this.btnEditEmail);
            this.pnlStart.Controls.Add(this.btnCreateOrder);
            this.pnlStart.Controls.Add(this.btnHistory);
            this.pnlStart.Location = new System.Drawing.Point(13, 13);
            this.pnlStart.Name = "pnlStart";
            this.pnlStart.Size = new System.Drawing.Size(471, 58);
            this.pnlStart.TabIndex = 2;
            // 
            // btnEditEmail
            // 
            this.btnEditEmail.Location = new System.Drawing.Point(314, 3);
            this.btnEditEmail.Name = "btnEditEmail";
            this.btnEditEmail.Size = new System.Drawing.Size(154, 55);
            this.btnEditEmail.TabIndex = 2;
            this.btnEditEmail.Text = "Konfiguracja e-mail";
            this.btnEditEmail.UseVisualStyleBackColor = true;
            this.btnEditEmail.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pnlCreateCustomer
            // 
            this.pnlCreateCustomer.Location = new System.Drawing.Point(12, 77);
            this.pnlCreateCustomer.Name = "pnlCreateCustomer";
            this.pnlCreateCustomer.Size = new System.Drawing.Size(472, 590);
            this.pnlCreateCustomer.TabIndex = 3;
            this.pnlCreateCustomer.Visible = false;
            this.pnlCreateCustomer.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlCreateCustomer_Paint);
            // 
            // pnlSuccess
            // 
            this.pnlSuccess.Controls.Add(this.label1);
            this.pnlSuccess.Location = new System.Drawing.Point(13, 77);
            this.pnlSuccess.Name = "pnlSuccess";
            this.pnlSuccess.Size = new System.Drawing.Size(471, 590);
            this.pnlSuccess.TabIndex = 4;
            this.pnlSuccess.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(73, 275);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Stworzono zamówienie";
            // 
            // pnlHistoryOrder
            // 
            this.pnlHistoryOrder.Location = new System.Drawing.Point(12, 77);
            this.pnlHistoryOrder.Name = "pnlHistoryOrder";
            this.pnlHistoryOrder.Size = new System.Drawing.Size(472, 590);
            this.pnlHistoryOrder.TabIndex = 5;
            // 
            // pnlEditEmail
            // 
            this.pnlEditEmail.Controls.Add(this.textBoxPassword);
            this.pnlEditEmail.Controls.Add(this.label7);
            this.pnlEditEmail.Controls.Add(this.button1);
            this.pnlEditEmail.Controls.Add(this.textBoxSubject);
            this.pnlEditEmail.Controls.Add(this.textBoxTo);
            this.pnlEditEmail.Controls.Add(this.textBoxFrom);
            this.pnlEditEmail.Controls.Add(this.textBoxPort);
            this.pnlEditEmail.Controls.Add(this.textBoxSMTP);
            this.pnlEditEmail.Controls.Add(this.label6);
            this.pnlEditEmail.Controls.Add(this.label5);
            this.pnlEditEmail.Controls.Add(this.label4);
            this.pnlEditEmail.Controls.Add(this.label3);
            this.pnlEditEmail.Controls.Add(this.label2);
            this.pnlEditEmail.Location = new System.Drawing.Point(12, 77);
            this.pnlEditEmail.Name = "pnlEditEmail";
            this.pnlEditEmail.Size = new System.Drawing.Size(472, 590);
            this.pnlEditEmail.TabIndex = 6;
            this.pnlEditEmail.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(93, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Zapisz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // textBoxSubject
            // 
            this.textBoxSubject.Location = new System.Drawing.Point(68, 84);
            this.textBoxSubject.Name = "textBoxSubject";
            this.textBoxSubject.Size = new System.Drawing.Size(171, 20);
            this.textBoxSubject.TabIndex = 9;
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(68, 64);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(171, 20);
            this.textBoxTo.TabIndex = 8;
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.Location = new System.Drawing.Point(68, 44);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.Size = new System.Drawing.Size(171, 20);
            this.textBoxFrom.TabIndex = 7;
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(68, 24);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(171, 20);
            this.textBoxPort.TabIndex = 6;
            // 
            // textBoxSMTP
            // 
            this.textBoxSMTP.Location = new System.Drawing.Point(68, 4);
            this.textBoxSMTP.Name = "textBoxSMTP";
            this.textBoxSMTP.Size = new System.Drawing.Size(171, 20);
            this.textBoxSMTP.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(5, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Temat:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(5, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "od:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(5, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "do:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(5, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Port:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(5, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "SMTP:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(5, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 11;
            this.label7.Text = "Hasło:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(68, 104);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(171, 20);
            this.textBoxPassword.TabIndex = 12;
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 679);
            this.Controls.Add(this.pnlEditEmail);
            this.Controls.Add(this.pnlHistoryOrder);
            this.Controls.Add(this.pnlSuccess);
            this.Controls.Add(this.pnlCreateCustomer);
            this.Controls.Add(this.pnlStart);
            this.Name = "Start";
            this.Text = "Form1";
            this.pnlStart.ResumeLayout(false);
            this.pnlSuccess.ResumeLayout(false);
            this.pnlSuccess.PerformLayout();
            this.pnlEditEmail.ResumeLayout(false);
            this.pnlEditEmail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateOrder;
        private System.Windows.Forms.Button btnHistory;
        private System.Windows.Forms.Panel pnlStart;
        private System.Windows.Forms.Panel pnlCreateCustomer;
        private System.Windows.Forms.Panel pnlSuccess;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlHistoryOrder;
        private System.Windows.Forms.Button btnEditEmail;
        private System.Windows.Forms.Panel pnlEditEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSubject;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.TextBox textBoxFrom;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.TextBox textBoxSMTP;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label7;
    }
}

