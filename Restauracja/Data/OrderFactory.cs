﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Restauracja.Data
{
    class OrderFactory
    {

        public OrderFactory()
        {

        }

        public List<Order> getAll()
        {
            List<Order> listOrder = new List<Order>();
            ConnectionData connectionData = new ConnectionData();

            string query = "SELECT * from dbo.Orders";

            SqlCommand sqlCommand = new SqlCommand(query);

            SqlDataReader sqlDataReader = connectionData.SendInquiry(sqlCommand);

            if (sqlDataReader.HasRows)
            {
                do
                {
                    listOrder.Add(new Order(sqlDataReader.GetInt32(0), sqlDataReader.GetString(1), sqlDataReader.GetString(2), sqlDataReader.GetString(3), sqlDataReader.GetBoolean(4)));
                } while (sqlDataReader.Read());
            }


            return listOrder;
        }

        public Order create(Order order)
        {
            ConnectionData connectionData = new ConnectionData();

            string query = "INSERT INTO Orders (Products, Counts, Message, IsDeleted) VALUES (@products, @counts, @message, @isdeleted);";

            SqlCommand sqlCommand = new SqlCommand(query);
            sqlCommand.Parameters.AddWithValue("@products", order.Products);
            sqlCommand.Parameters.AddWithValue("@counts", order.Counts);
            sqlCommand.Parameters.AddWithValue("@message", order.Message);
            sqlCommand.Parameters.AddWithValue("@isdeleted", false);

            SqlDataReader sqlDataReader = connectionData.SendInquiry(sqlCommand);

            if (sqlDataReader.HasRows)
            {
                do
                {

                } while (sqlDataReader.Read());
            }


            return order;
        }
    }
}
