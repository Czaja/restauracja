﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restauracja.Data
{
    class Category
    {
        private int _id;
        private string _name;
        private bool _isdeleted;

        public Category(int id, string name, bool isdeleted)
        {
            ID = id;
            Name = name;
            IsDeleted = isdeleted;
        }

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return _isdeleted;
            }
            set
            {
                _isdeleted = value;
            }
        }
    }
}
