﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Restauracja.Data
{
    class EmailFactory
    {
        public EmailFactory()
        {

        }

        public List<Email> getAll()
        {
            List<Email> listEmail = new List<Email>();
            ConnectionData connectionData = new ConnectionData();

            string query = "SELECT * from dbo.Email";

            SqlCommand sqlCommand = new SqlCommand(query);

            SqlDataReader sqlDataReader = connectionData.SendInquiry(sqlCommand);

            if (sqlDataReader.HasRows)
            {
                do
                {
                    listEmail.Add(new Email(sqlDataReader.GetInt32(0), sqlDataReader.GetString(1), sqlDataReader.GetString(2), sqlDataReader.GetString(3), sqlDataReader.GetString(4), sqlDataReader.GetString(5), sqlDataReader.GetString(6)));
                } while (sqlDataReader.Read());
            }


            return listEmail;
        }

        public Email update(Email email)
        {
            List<Email> listEmail = new List<Email>();
            ConnectionData connectionData = new ConnectionData();

            string query = "UPDATE dbo.Email Set Host = @host, Port = @port, Too = @to, Fromm = @from, Subject = @subject, Password = @password Where id = '1'";

            SqlCommand sqlCommand = new SqlCommand(query);
            sqlCommand.Parameters.AddWithValue("@host", email.Host);
            sqlCommand.Parameters.AddWithValue("@port", email.Port);
            sqlCommand.Parameters.AddWithValue("@to", email.To);
            sqlCommand.Parameters.AddWithValue("@from", email.From);
            sqlCommand.Parameters.AddWithValue("@subject", email.Subject);
            sqlCommand.Parameters.AddWithValue("@password", email.Password);

            SqlDataReader sqlDataReader = connectionData.SendInquiry(sqlCommand);

            if (sqlDataReader.HasRows)
            {
                do
                {

                } while (sqlDataReader.Read());
            }


            return email;
        }
    }
}
