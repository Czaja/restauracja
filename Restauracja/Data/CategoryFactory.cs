﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Restauracja.Data
{
    class CategoryFactory
    {
        public CategoryFactory()
        {

        }

        public List<Category> getAll()
        {
            List<Category> listCategory = new List<Category>();
            ConnectionData connectionData = new ConnectionData();

            string query = "SELECT * from dbo.Category";

            SqlCommand sqlCommand = new SqlCommand(query);

            SqlDataReader sqlDataReader = connectionData.SendInquiry(sqlCommand);

            if (sqlDataReader.HasRows)
            {
                do
                {
                    listCategory.Add(new Category(sqlDataReader.GetInt32(0), sqlDataReader.GetString(1), sqlDataReader.GetBoolean(2)));
                } while (sqlDataReader.Read());
            }


            return listCategory;
        }
    }
}
