﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restauracja.Data
{
    class Order
    {
        private int _id;
        private string _products;
        private string _counts;
        private string _message;
        private bool _isdeleted;

        public Order(int id, string products, string counts, string message, bool isdeleted)
        {
            ID = id;
            Products = products;
            Counts = counts;
            Message = message;
            IsDeleted = isdeleted;
        }

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
            }
        }

        public string Counts
        {
            get
            {
                return _counts;
            }
            set
            {
                _counts = value;
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return _isdeleted;
            }
            set
            {
                _isdeleted = value;
            }
        }
    }
}
