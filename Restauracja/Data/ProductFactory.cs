﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Restauracja.Data
{
    class ProductFactory
    {
        public ProductFactory()
        {

        }

        public List<Product> getAll()
        {
            List<Product> listProduct = new List<Product>();
            ConnectionData connectionData = new ConnectionData();

            string query = "SELECT * from dbo.Product";

            SqlCommand sqlCommand = new SqlCommand(query);

            SqlDataReader sqlDataReader = connectionData.SendInquiry(sqlCommand);

            if (sqlDataReader.HasRows)
            {
                do
                {
                    Constant.TypeProduct typeProduct = Constant.TypeProduct.Main;
                    if (sqlDataReader.GetString(4).Equals("E"))
                    {
                        typeProduct = Constant.TypeProduct.Extra;
                    }
                    listProduct.Add(new Product(sqlDataReader.GetInt32(0), sqlDataReader.GetString(1), (float)sqlDataReader.GetDouble(2), sqlDataReader.GetInt32(3), typeProduct, sqlDataReader.GetBoolean(5)));
                } while (sqlDataReader.Read());
            }


            return listProduct;
        }
    }
}
