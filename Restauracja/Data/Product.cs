﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restauracja.Data
{
    class Product
    {
        private int _id;
        private string _name;
        private float _price;
        private int _category;
        private Constant.TypeProduct _typeproduct;
        private bool _isdeleted;

        public Product(int id, string name, float price, int category, Constant.TypeProduct typeProduct,  bool isdeleted)
        {
            ID = id;
            Name = name;
            Price = price;
            Category = category;
            TypeProduct = typeProduct;
            IsDeleted = isdeleted;
        }

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public float Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }

        public int Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }

        public Constant.TypeProduct TypeProduct
        {
            get
            {
                return _typeproduct;
            }
            set
            {
                _typeproduct = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return _isdeleted;
            }
            set
            {
                _isdeleted = value;
            }
        }
    }
}
