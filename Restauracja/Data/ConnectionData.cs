﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Restauracja.Data
{
    class ConnectionData
    {
        private String strConnection = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=E:\\Projekt\\Restauracja\\Restauracja\\Data\\Baza_danych.mdf;Integrated Security=True";

        SqlConnection sqlConnection;

        public ConnectionData()
        {
            sqlConnection = new SqlConnection(strConnection);
        }

        public SqlDataReader SendInquiry(SqlCommand sqlCommand)
        {
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();

            SqlDataReader reader = sqlCommand.ExecuteReader();
            reader.Read();

            return reader;
        }
    }
}
